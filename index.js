const express = require('express');
const app = express();
const routes = require('./routes');

app.use(routes)

// app.get('/people',
//     findPeople
// )
// function findPeople(req, res, next) {

//     async function fetchPeople() {
//         const response = await fetch('https://swapi.dev/api/people/');
//         const body = await response.text();
//         res.send(body)
//     }
//     fetchPeople()

// }

const server = app.listen(3000, () => {
    const host = server.address().address,
        port = server.address().port;

    console.log('API listening at http://%s:%s', host, port);
});
