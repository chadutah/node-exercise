var router = require("express").Router();
var collection = require('fetch-collection')
 

 
router.get('/:sortBy?',
findPeople
)
function findPeople(req, res, next) {
    let peeps = []
    collection('https://swapi.dev/api/people/', {
  q: 'language:javascript',
  sort: "name",
  order: 'desc'
}).fetch('json', {
  data: (response, body) => body.results, // return an array of data
  next: (response, body) => body.next, // return a string (URL) or an object (params)
}).on('data', item => {
  // do something with item
  peeps.push(item)
}).on('end', function () {
    // res.send(peeps)
    console.log(req.params)
    if (req.params.sortBy === 'name') {
        peeps.sort((a,b) => {
            if(a === b) {
                return 0;
            }
            return a.name < b.name ? -1 : 1
        })
        res.send(peeps)
    }
    if (req.params.sortBy === 'height') {
        res.send(peeps.sort((a,b) => b.height - a.height))
    }
    if (req.params.sortBy === 'mass') {
        res.send(peeps.sort((a,b) => b.mass - a.mass))
    }
    if (req.params.sortBy === undefined) {
        res.send(peeps)
    }
})
    
    // async function fetchPeople() {
    //     const { items } = await fetchPaginate.fetchPaginate("https://swapi.dev/api/people/");
    //     // const response = await fetch('https://swapi.dev/api/people/');
    //     // const body = await response.text();
    //     // res.send(body)
    //     res.send(items)
    // }
    // fetchPeople()

}

module.exports = router;