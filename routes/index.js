var path = require("path");
var router = require("express").Router();
var peopleRoutes = require ("./people");
var planetRoutes= require ("./planets");

// API Routes
router.use("/people", peopleRoutes);

// HTML Roues
router.use("/planets", planetRoutes);



module.exports = router;