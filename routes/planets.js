var router = require("express").Router();
var collection = require('fetch-collection')
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));

router.get('/',
    findPlanets
)
function findPlanets(req, res, next) {

    let planets = []
    collection('https://swapi.dev/api/planets/', {
        q: 'language:javascript',
        sort: "name",
        order: 'desc'
    }).fetch('json', {
        data: (response, body) => body.results, // return an array of data
        next: (response, body) => body.next, // return a string (URL) or an object (params)
    }).on('data', item => {
        // do something with item
        planets.push(item)
    }).on('end', async function () {
        function callback() {
            res.send(planets)
        }
        var itemsProcessed = 0
        async function changeName() {
            planets.forEach(element => {
                async function switchName() {
                    let residentNames = []
                    for (const resident of element.residents) {
                        const response = await fetch(resident);
                        const data = await response.json();
                        residentNames.push(data.name)
                    }
                    element.residents = residentNames
                }
                itemsProcessed++;
                switchName()
                    ;
                if (itemsProcessed === planets.length) {
                    // console.log(planets)
                    setTimeout(() => {
                        callback()
                    }, 10000);
                }
            });
        }
        changeName()
    })

}


module.exports = router;